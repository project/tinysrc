
-- SUMMARY --

This module lets you filter inline images in node content via the tinySrc 
service. The tinySrc service automatically resizes and serves images 
optimised for the current device viewing the content, in particular 
mobile devices.

For more information go to: http://tinysrc.net/

For a full description of the module, visit the project page:
  http://drupal.org/project/tinysrc

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/tinysrc

-- INSTALLATION --

* Install as usual, unzip the archive into your sites/all/modules
  directory

-- CONFIGURATION --

* Once the module has been installed and enabled go to: Site
  configuration > Input formats > List.

* On the format you wish to add the filter to, click "configure".

* In the "Filters" fieldset, check the tick box for "tinySrc filter".
  If you don't need to configure any rules relating to the tweaking
  the size of the image returned, you're done! If not continue reading.

* The page should reload, now click on the "Configure" tab (in between
  the "Edit" and "Rearrange" tabs).

* On this page in the "tinySrc filter" fieldset you have textfields which
  let you tweak the width and/or height of the result image that the
  tinySrc service returns, and also the file format of the result image.
  Make any changes you need then click "Save configuration" at the end of
  the page.

NOTE: Settings for adjusting the width/height or result image format must
      be set per Input format which uses the tinySrc filter.


-- CONTACT --

Current maintainers:
* Alli Price (alli.price) - http://drupal.org/user/431193

This project has been sponsored by:
* Deeson Online
  Visit http://www.deeson.co.uk/online for more information.
      
      